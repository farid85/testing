import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.*;

public class MultiplyTest {

    SimpleMultiplier multiplier;

    @Before
    public void Setup()
    {
       multiplier = new SimpleMultiplier(); 
    }

    @Test
    public void baseMultiplicationTests() {
        assertEquals(  "10 x 1 must be 10", 10, multiplier.multiply(10, 1) );
        assertEquals(  "0 x 10 must be 0", 0, multiplier.multiply(0, 10) );
        assertEquals( "0 x 0 must be 0", 0, multiplier.multiply(0, 0) );
    }
}
